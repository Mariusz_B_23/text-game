package room;

public class Office implements RoomInterface{


    @Override
    public void describe() {

        System.out.println("""
                
                --------------------------------------------------------------------------------------------------------
                
                You find yourself in a room full of desks, familiar to you from large corporations.
                There is no one in it, silence reigns.
                At the end of the room by the last desk, there is a TV.
                On the desk stands the only one switched on in the whole office.
                
                --------------------------------------------------------------------------------------------------------
                """);

    }


}

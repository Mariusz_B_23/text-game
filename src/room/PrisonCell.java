package room;

public class PrisonCell implements RoomInterface {


    @Override
    public void describe() {

        System.out.println("""
                
                ________________________________________________________________________________________________________
                
                You are in a prison cell.
                However, this is no ordinary cell,
                as you can get out of here because the lock has a PIN code that you can also enter inside.
                The cell is 4x5m wide. It contains only a bed, and a desk with a chair.
                There is a notebook on the desk and a pen next to it.
                
                --------------------------------------------------------------------------------------------------------
                 """);

    }


}

package game;

import exceptions.PersonIsNullException;
import item.CommandList;
import item.ItemManager;
import player.Player;
import room.Office;

import java.util.Scanner;

public class ChapterTwo implements ChapterInterface {

    private static final Scanner KEYBOARD = new Scanner(System.in);
    private static final Office OFFICE = new Office();


    @Override
    public void gameplay(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        startMessage(player);
        userReturnsPilotAndTurnsOnTelevision(player);
        explanationOfTask(player);
        userWritesPassword();
        farewellMessage(player);

    }


    @Override
    public void startMessage(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        try {
            Thread.sleep(10000);
            System.out.println("""
                           
                           
                                
                    LOADING SECOND CHAPTER...
                                
                                
                    """);
            Thread.sleep(4000);

            System.out.println("""
                    --------------------------------------------------------------------------------------------------------
                                        
                    You left the cell. The hardest part is behind you.
                    I know you may be surprised at where you are now,
                    but you are one step away from getting your life back.
                    I know you may feel a bit apprehensive,
                    but if you want to find out where you are choose one of the available commands.
                    I will now remind you of all the commands....
                    """);
            CommandList.commandList();
            System.out.println("""
                                    
                    I suggest, however, that you select command
                    "DESC"
                    at this point and you will learn a little more about where you are.
                    """);
            String userChoose;
            do {
                userChoose = KEYBOARD.nextLine().toUpperCase().trim();
                if (userChoose.equals(CommandList.DESC.name().toUpperCase())) {
                    OFFICE.describe();
                } else {
                    System.out.printf("""
                                                    
                            %s, did I not make it clear that you should press "DESC"?
                            I control everything here, so you'd better harmonise with me!
                            Once again, I politely ask you to choose "DESC".
                                                    
                            """, player.name());
                }
            } while (!userChoose.equals(CommandList.DESC.name()));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }


    private static void userReturnsPilotAndTurnsOnTelevision(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        try {
            Thread.sleep(8000);

            System.out.printf("""
                                        
                    %s go to the desk at the end of the room where the only computer on is standing.
                    As you can see on the screen you have a box where you have to enter your password.
                    If you enter the correct password, our paths will part,
                    we will never meet again and you will go back to your life.
                    Don't worry, you won't have to come up with a very complicated password.
                    Take the remote control and turn on the TV.
                                        
                    If you need help, type - "help"
                    To place it in the inventory type - "take tv remote control"
                                        
                    """, player.name());

            takeTvRemoteControl();
            useTvRemoteControl(player);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }


    private static void takeTvRemoteControl() {

        String userCommand;

        do {
            userCommand = KEYBOARD.nextLine().toUpperCase().trim();
            if (userCommand.equals(CommandList.TAKE.name().toUpperCase() + " tv remote control".toUpperCase())) {
                ItemManager.addTvRemoteControl();
            } else if (userCommand.equals(CommandList.HELP.name().toUpperCase())) {
                CommandList.commandList();
            } else if (userCommand.equals(CommandList.DESC.name().toUpperCase())) {
                OFFICE.describe();
            } else if (userCommand.equals(CommandList.ITEMS.name().toUpperCase())) {
                ItemManager.showItemList();
                System.out.println("\nTo place it in the inventory type - \"take tv remote control\"");
            } else {
                System.out.println("Command not available at the moment.\n");
            }
        } while (!userCommand.equals(CommandList.TAKE.name().toUpperCase() + " tv remote control".toUpperCase()));

    }


    private static void useTvRemoteControl(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        System.out.printf("""
                                
                %s super!
                The TV remote control is in your inventory!
                To check it out type - "items"
                """, player.name());

        String userCommand;
        System.out.println(""" 
                If you need help, type - "help"
                To use an object, in this case to open a manual, type - "use tv remote control"
                              
                """);
        do {
            userCommand = KEYBOARD.nextLine().toUpperCase().trim();
            if (userCommand.equals(CommandList.USE.name().toUpperCase() + " tv remote control".toUpperCase())) {
                ItemManager.useTvRemoteControl();
            } else if (userCommand.equals(CommandList.HELP.name().toUpperCase())) {
                CommandList.commandList();
            } else if (userCommand.equals(CommandList.DESC.name().toUpperCase())) {
                OFFICE.describe();
            } else if (userCommand.equals(CommandList.ITEMS.name().toUpperCase())) {
                ItemManager.showItemList();
                System.out.println("\nTo use an object, in this case to open a manual, type - \"use tv remote control\"");
            } else {
                System.out.println("Command not available at the moment.\n");
            }
        } while (!userCommand.equals(CommandList.USE.name().toUpperCase() + " tv remote control".toUpperCase()));

    }


    private static void explanationOfTask(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        System.out.printf("""
                                
                %s you saw four lines of different characters on the screen.
                If you type them in the correct order into the box that appears on your computer monitor,
                our paths will diverge. You don't have a trial limit, and at the end I'll tell you why.
                Good luck!!!
                
                
                """, player.name());

    }


    private static void userWritesPassword() {

        String password = "m1F&S@b4#z9C5R*k";
        String userPassword;

        do {
            System.out.println("Enter the correct password: \n");
            userPassword = KEYBOARD.nextLine();
            if (!userPassword.equals(password)) {
                System.out.println("The password is incorrect, try again.");
                System.out.println("""
                        S@b4
                        #z9C
                        m1F&
                        5R*k
                                """);
            }
        } while (!userPassword.equals(password));

        System.out.println("\n----- CORRECT PASSWORD -----");

    }


    private static void farewellMessage(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        try {
            Thread.sleep(5000);
            System.out.printf("""
                                    
                    %s this is where our adventure ends.
                    I am glad because you have learned how to use the binary sorting algorithm,
                    and you have seen what a strong password should look like.
                    It was because of your weak security that I was able to hack into your computer.
                    Don't forget what happened here and set strong passwords everywhere!!!
                    
                    
                    """, player.name());

            Thread.sleep(5000);
            System.out.println("The installation of the full version of the antivirus has started....");
            Thread.sleep(1000);
            System.out.println("Antivirus successfully installed.");
            Thread.sleep(1000);
            System.out.println("The threat of viral infection has been removed...");
            Thread.sleep(1000);
            System.out.println("The user regained full control of the computer.");
            Thread.sleep(1000);
            System.out.println("----------------------------------------------------");
            Thread.sleep(1000);
            System.out.println("-------------------------------------------");
            Thread.sleep(1000);
            System.out.println("------------------------------");
            Thread.sleep(1000);
            System.out.println("--------------------");
            Thread.sleep(2000);
            System.out.println("""
                                        
                    ------------------------->     THE END :D     <-------------------------
                                        
                    """);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

}

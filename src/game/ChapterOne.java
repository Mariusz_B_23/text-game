package game;

import exceptions.*;
import item.CommandList;
import item.ItemManager;
import player.Player;
import room.PrisonCell;

import java.util.Random;
import java.util.Scanner;

public class ChapterOne implements ChapterInterface {

    private static final Scanner KEYBOARD = new Scanner(System.in);
    private static final Random RANDOM = new Random();
    private static final PrisonCell PRISON_CELL = new PrisonCell();


    @Override
    public void gameplay(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        startMessage(player);
        firstUserPinCodeGuessTry(player);
        messagePinCodeGuessFirstAttempt(player);
        findAndUseInstruction(player);
        messageBeforeResumingSecondIdealPinCode(player);
        mechanismForStateGuessPinCode(player);

    }

    @Override
    public void startMessage(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        try {
            Thread.sleep(3000);
            System.out.println("""
                           
                           
                                
                    LOADING FIRST CHAPTER...
                    """);
            Thread.sleep(4000);

            System.out.println("""
                    --------------------------------------------------------------------------------------------------------
                    This is where we start our adventure.
                    I know you may feel a bit apprehensive,
                    but if you want to find out where you are choose one of the available commands.
                    I will now remind you of all the commands....
                    """);
            CommandList.commandList();
            System.out.println("""
                                    
                    I suggest, however, that you select command
                    "DESC"
                    at this point and you will learn a little more about where you are.
                    """);
            String userChoose;
            do {
                userChoose = KEYBOARD.nextLine().toUpperCase().trim();
                if (userChoose.equals(CommandList.DESC.name().toUpperCase())) {
                    PRISON_CELL.describe();
                } else {
                    System.out.printf("""
                                                    
                            %s, did I not make it clear that you should press "DESC"?
                            I control everything here, so you'd better harmonise with me!
                            Once again, I politely ask you to choose "DESC".
                                                    
                            """, player.name());
                }
            } while (!userChoose.equals(CommandList.DESC.name()));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }


    private static void firstUserPinCodeGuessTry(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        try {
            Thread.sleep(7000);
            System.out.println("""
                                    
                    Your task is to get out of this cell.
                    The PIN code is a number between 100 and 999, and now the most important thing.
                    You have two attempts, and in each attempt you can enter the wrong code a maximum of nine times.
                    If you get it wrong ten times on the first attempt then the PIN will change.
                    If on the second attempt you enter the wrong PIN ten times then
                    ... all data from your computer will be deleted, but first all passwords that you use will be copied.
                                    """);
            Thread.sleep(10000);
            System.out.printf(""" 
                    Game on %s! Try to guess the PIN code and escape the cell ...
                                    
                    """, player.name());

            firstTrialWithInvalidCode(player);

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }


    private static void firstTrialWithInvalidCode(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        try {
            Thread.sleep(5000);
            int pinCode = 99;
            System.out.println("...PIN CODE HAS BEEN UPDATED...\n");
            Thread.sleep(3000);
            int attemptCount = 0;
            String regex = "^[1-9][0-9][0-9]$";
            checkPinCodeFirstAttempt(player, pinCode, attemptCount, regex);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }


    private static void checkPinCodeFirstAttempt(Player player, int pinCode, int attemptCount, String regex) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        if (pinCode <= 0 || pinCode >= 1000) {
            throw new WrongPinCodeException("The PIN must not be less than 1 and greater than 1000!");
        }

        if (attemptCount <= -1) {
            throw new AttemptCountExceptionCannotBeLessThanZero("The number of attempts must not be less than 0!");
        }

        if (regex.isEmpty()) {
            throw new RegexCannotBeEmptyException("Regex is empty!");
        }

        String userPin;
        do {

            userPin = getCodePinFromUser(regex);
            attemptCount = getAttemptCount(pinCode, attemptCount, userPin);

        } while (!(pinCode == Integer.parseInt(userPin)) && !(attemptCount == 10));
        if (attemptCount == 10) {
            System.out.printf("""
                    %s you have reached the limit of ten attempts.
                    You have failed to open the door.
                    PIN CODE was: %d
                    """, player.name(), pinCode);
        }

    }


    private static String getCodePinFromUser(String regex) {

        if (regex.isEmpty()) {
            throw new RegexCannotBeEmptyException("Regex is empty!");
        }

        String userPin;
        do {
            System.out.println("""
                    ---------------------------------------------------------------------------------------------------
                    Enter a number between 100 and 999.
                    """);
            userPin = KEYBOARD.nextLine();
            if (!userPin.matches(regex)) {
                System.out.println("Wrong combination. Enter a number between 100 and 999.\n");
            }
        } while (!userPin.matches(regex));
        return userPin;

    }


    private static int getAttemptCount(int pinCode, int attemptCount, String userPin) {

        if (pinCode <= 0 || pinCode >= 1000) {
            throw new WrongPinCodeException("The PIN must not be less than 1 and greater than 1000!");
        }

        if (attemptCount <= -1) {
            throw new AttemptCountExceptionCannotBeLessThanZero("The number of attempts must not be less than 0!");
        }

        if (userPin == null || userPin.isEmpty() ||userPin.isBlank()) {
            throw new UserPinAsStringCannotBeNullOrEmpty("User pin cannot be null or empty!");
        }

        if (pinCode == Integer.parseInt(userPin)) {
            System.out.printf("""
                    Success. You have entered the correct PIN code!!!
                    PIN code: %d
                    """, pinCode);
        } else if (pinCode > Integer.parseInt(userPin)) {
            System.out.println("\nThe PIN code is a ↑↑↑ BIGGER ↑↑↑ number than the one you specified....");
            attemptCount++;
            System.out.println("Attempt count: " + attemptCount + ".\n");
        } else if (pinCode < Integer.parseInt(userPin)) {
            System.out.println("\nThe PIN code is a ↓↓↓ SMALLER ↓↓↓ number than the one you specified....");
            attemptCount++;
            System.out.println("Attempt count: " + attemptCount + ".\n");
        }
        return attemptCount;

    }


    private static void messagePinCodeGuessFirstAttempt(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        try {
            Thread.sleep(5000);
            System.out.printf("""
                                
                    ----------------------------------------------------------------------------------------------------
                                       
                    %s I have to be honest with you.
                    If you entered all the numbers from 100 to 999, you still wouldn't guess the PIN.
                    Code PIN was 99.
                    Don't get upset, I just want to teach you something.
                    Open the desk drawer, there is a document there that will help you get out
                    of your cell in up to 10 attempts.
                                        
                    ----------------------------------------------------------------------------------------------------
                                        
                    """, player.name());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }


    private static void findAndUseInstruction(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        try {
            Thread.sleep(4000);
            System.out.println("""
                                
                    To open a drawer, type - "open drawer"
                    """);
            openDrawer();
            System.out.printf("""
                                    
                    %s the drawer contains instructions for the use of binary.
                    If you need help, type - "help"
                    To place it in the inventory type - "take instruction"
                                    
                    """, player.name());
            takeInstructionBinarySearch();
            useInstructionBinarySearch(player);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }


    private static void openDrawer() {

        String commandOpenDrawer = "open drawer";
        String userCommand;
        do {
            userCommand = KEYBOARD.nextLine().toLowerCase().trim();
            if (!userCommand.equals(commandOpenDrawer)) {
                System.out.println("Inappropriate command. Type -  \"open drawer\"");
            }
        } while (!userCommand.equals(commandOpenDrawer));

    }


    private static void takeInstructionBinarySearch() {

        String userCommand;

        do {
            userCommand = KEYBOARD.nextLine().toUpperCase().trim();
            if (userCommand.equals(CommandList.TAKE.name().toUpperCase() + " instruction".toUpperCase())) {
                ItemManager.addBinaryInstruction();
            } else if (userCommand.equals(CommandList.HELP.name().toUpperCase())) {
                CommandList.commandList();
            } else if (userCommand.equals(CommandList.DESC.name().toUpperCase())) {
                PRISON_CELL.describe();
            } else if (userCommand.equals(CommandList.ITEMS.name().toUpperCase())) {
                ItemManager.showItemList();
                System.out.println("\nTo place it in the inventory type - \"take instruction\"");
            } else {
                System.out.println("Command not available at the moment.\n");
            }
        } while (!userCommand.equals(CommandList.TAKE.name().toUpperCase() + " instruction".toUpperCase()));

    }


    private static void useInstructionBinarySearch(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        System.out.printf("""
                                
                %s super!
                There is an instruction manual in your equipoise!
                To check it out type - "items"
                """, player.name());

        String userCommand;
        System.out.println(""" 
                If you need help, type - "help"
                To use an object, in this case to open a manual, type - "use instruction"
                              
                """);
        do {
            userCommand = KEYBOARD.nextLine().toUpperCase().trim();
            if (userCommand.equals(CommandList.USE.name().toUpperCase() + " instruction".toUpperCase())) {
                ItemManager.useBinaryInstruction();
            } else if (userCommand.equals(CommandList.HELP.name().toUpperCase())) {
                CommandList.commandList();
            } else if (userCommand.equals(CommandList.DESC.name().toUpperCase())) {
                PRISON_CELL.describe();
            } else if (userCommand.equals(CommandList.ITEMS.name().toUpperCase())) {
                ItemManager.showItemList();
                System.out.println("\nTo use an object, in this case to open a manual, type - \"use instruction\"");
            } else {
                System.out.println("Command not available at the moment.\n");
            }
        } while (!userCommand.equals(CommandList.USE.name().toUpperCase() + " instruction".toUpperCase()));

    }


    private static void messageBeforeResumingSecondIdealPinCode(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        try {
            Thread.sleep(15000);
            System.out.printf("""
                                    
                    Ok %s, now you know what binary sorting is all about. This time, seriously,
                    the PIN code will be a number between 100 and 999,
                    and I guarantee you that we will be able to crack it,
                    acting according to the instructions.
                    Also use a notebook and pen to calculate the results.
                    Get to it!!!
                                    
                    """, player.name());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }


    private static boolean secondExampleGuessPinCode(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        boolean check;
        try {
            Thread.sleep(5000);
            int pinCode = RANDOM.nextInt(900) + 100;
            System.out.println("...PIN CODE HAS BEEN UPDATED...\n");
            Thread.sleep(3000);
            int attemptCount = 0;
            String regex = "^[1-9][0-9][0-9]$";
            check = checkPinCodeSecondAttempt(player, pinCode, attemptCount, regex);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return check;

    }


    private static boolean checkPinCodeSecondAttempt(Player player, int pinCode, int attemptCount, String regex) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        if (pinCode <= 0 || pinCode >= 1000) {
            throw new WrongPinCodeException("The PIN must not be less than 1 and greater than 1000!");
        }

        if (attemptCount <= -1) {
            throw new AttemptCountExceptionCannotBeLessThanZero("The number of attempts must not be less than 0!");
        }

        if (regex.isEmpty()) {
            throw new RegexCannotBeEmptyException("Regex is empty!");
        }

        String userPin;
        boolean successful = false;
        do {
            if (attemptCount == 0) {
                System.out.printf("""
                                                
                        %s the range of possible answers is between 100 and 999.
                        You determine the midpoint --> (100 + 999) / 2 ... 1099 / 2 = 549.5 -> 549
                        So 549 this should be the first number you specify.
                        Divide and conquer!!!
                                                
                        """, player.name());
            }

            userPin = getCodePinFromUser(regex);
            attemptCount = getAttemptCount(pinCode, attemptCount, userPin);


        } while (!(pinCode == Integer.parseInt(userPin)) && !(attemptCount == 10));

        if (attemptCount < 10) {
            successful = true;
        }

        if (attemptCount == 10) {
            System.out.printf("""
                    %s you have reached the limit of ten attempts.
                    You have failed to open the door.
                    Surprise!!! PIN CODE was: %d
                    """, player.name(), pinCode);
        }
        return successful;

    }


    private static void automaticBinarySortedAlgorithm(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        int pinCode = 777;
        int min = 100;
        int max = 999;
        int userNumber;
        int help;
        int help2;

        try {
            Thread.sleep(10000);

            System.out.printf("""
                                    
                    --------------------------------------------------------------------------------------------------------
                                    
                    %s you have let me down! You were given instructions on how to get a PIN code.
                    I told you that I would delete all the data from your computer,
                    but before that I would copy all the passwords and steal your identity, and you didn't care at all.
                    I liked you, so I'm going to show you how to get the PIN code, and then you get one last chance.
                    The range of numbers is exactly the same, from 100 to 999.
                    Let's assume that the PIN will be the number %d....
                                    
                    """, player.name(), pinCode);

            Thread.sleep(11500);

            System.out.println("""
                    ###################################################################################################
                    1. The range of possible answers is between 100 and 999. You determine the midpoint.
                    (100 + 999) / 2 ... 1099 / 2 = 549.5 -> 549
                                    
                    """);

            userNumber = (min + max) / 2;

            Thread.sleep(11500);

            System.out.println("""
                    ####################################################################################################
                    2. You get the message that the number 549 is smaller than the PIN (777),
                    so at this point (549 + 1 = ) 550 is your new minimum number!
                    Therefore, in the next step you narrow the interval and subtract the number you last entered
                    from the largest number in the interval.
                    999 - 550 = 449
                                    
                    The number you get is divided by 2.
                    449 / 2 =  224.5 -> 224
                                    
                    You subtract the previous dividing result from the highest number in the interval.
                    And this is the number you should give as the next.
                    999 - 224 = 775 <- your number
                                    
                    """);

            min = userNumber + 1;
            help = max - min;
            help2 = help / 2;
            userNumber = max - help2;

            Thread.sleep(11500);

            System.out.println("""
                    ####################################################################################################
                    3. You get the message that the number 775 is smaller than the PIN (777),
                    so at this point (775 + 1 = ) 776 is your new minimum number!
                    Therefore, in the next step you narrow the interval and subtract the number you last entered
                    from the largest number in the interval.
                    999 - 776 = 223
                                    
                    The number you get is divided by 2.
                    You round the number down.
                    223 / 2 = 111.5 -> 111
                                    
                    You subtract the previous dividing result from the highest number in the interval.
                    And this is the number you should give as the next.
                    999 - 111 = 888 <- your number
                                    
                    """);

            min = userNumber + 1;
            help = max - min;
            help2 = help / 2;
            userNumber = max - help2;

            Thread.sleep(11500);

            System.out.println("""
                    ####################################################################################################
                    4. You get the message that the number 888 is bigger than the PIN (777).
                    At this point you know that the new highest number in the interval is (888 - 1 = ) 887,
                    allowing us to narrow it down even further!
                    The minimum number is the last number you entered which was less than the PIN.
                    You must now subtract the largest number from the smallest number again.
                    887 - 776 = 111
                                    
                    The number you get is divided by 2.
                    You round the number down.
                    111 / 2 = 55.5 -> 55
                                    
                    You subtract the previous dividing result from the highest number in the interval.
                    And this is the number you should give as the next.
                    887 - 55 = 832 <- your number
                                    
                    """);

            max = userNumber - 1;
            help = max - min;
            help2 = help / 2;
            userNumber = max - help2;

            Thread.sleep(11500);

            System.out.println("""
                    ####################################################################################################
                    5. You get the message that the number 832 is bigger than the PIN (777).
                    At this point you know that the new highest number in the interval is (832 - 1 = ) 831,
                    allowing us to narrow it down even further!
                    The minimum number is the last number you entered which was less than the PIN.
                    You must now subtract the largest number from the smallest number again.
                    831 - 776 = 55
                                    
                    The number you get is divided by 2.
                    55 / 2 = 27.5 -> 27
                                    
                    You subtract the previous dividing result from the highest number in the interval.
                    And this is the number you should give as the next.
                    831 - 27 = 804 <- your number
                    """);

            max = userNumber - 1;
            help = max - min;
            help2 = help / 2;
            userNumber = max - help2;

            Thread.sleep(11500);

            System.out.println("""
                    ####################################################################################################
                    6. You get the message that the number 804 is bigger than the PIN (777).
                    At this point you know that the new highest number in the interval is (804 - 1 = ) 803,
                    allowing us to narrow it down even further!
                    The minimum number is the last number you entered which was less than the PIN.
                    You must now subtract the largest number from the smallest number again.
                    803 - 776 = 27
                                    
                    The number you get is divided by 2.
                    27 / 2 = 13.5 -> 13
                                    
                    You subtract the previous dividing result from the highest number in the interval.
                    And this is the number you should give as the next.
                    803 - 13 = 790 <- your number
                                    
                    """);

            max = userNumber - 1;
            help = max - min;
            help2 = help / 2;
            userNumber = max - help2;

            Thread.sleep(11500);

            System.out.println("""
                    ####################################################################################################
                    7. You get the message that the number 790 is bigger than the PIN (777).
                    At this point you know that the new highest number in the interval is (790 - 1 = ) 789,
                    allowing us to narrow it down even further!
                    The minimum number is the last number you entered which was less than the PIN.
                    You must now subtract the largest number from the smallest number again.
                    789 - 776 = 13
                                    
                    The number you get is divided by 2.
                    You round the number down.
                    13 / 2 = 6.5 -> 6
                                    
                    You subtract the previous dividing result from the highest number in the interval.
                    And this is the number you should give as the next.
                    789 - 6 = 783 <- your number
                                    
                    """);

            max = userNumber - 1;
            help = max - min;
            help2 = help / 2;
            userNumber = max - help2;

            Thread.sleep(11500);

            System.out.println("""
                    ####################################################################################################
                    8. You get the message that the number 783 is bigger than the PIN (777).
                    At this point you know that the new highest number in the interval is (783 - 1 = ) 782,
                    allowing us to narrow it down even further!
                    The minimum number is the last number you entered which was less than the PIN.
                    You must now subtract the largest number from the smallest number again.
                    782 - 776 = 6
                                    
                    The number you get is divided by 2.
                    6 / 2 = 3
                                    
                    You subtract the previous dividing result from the highest number in the interval.
                    And this is the number you should give as the next.
                    782 - 3 = 779 <- your number
                    """);

            max = userNumber - 1;
            help = max - min;
            help2 = help / 2;
            userNumber = max - help2;

            Thread.sleep(11500);

            System.out.println("""
                    ####################################################################################################
                    9. You get the message that the number 779 is bigger than the PIN (777).
                    At this point you know that the new highest number in the interval is (779 - 1 = ) 778,
                    allowing us to narrow it down even further!
                    The minimum number is the last number you entered which was less than the PIN.
                    You must now subtract the largest number from the smallest number again.
                    778 - 776 = 2
                                    
                    The number you get is divided by 2.
                    2 / 2 = 1
                                    
                    You subtract the previous dividing result from the highest number in the interval.
                    And this is the number you should give as the next.
                    778 - 1 = 777 <- your number!!! SUCCESS!!
                                        
                    9 attempts and you have a PIN code!
                    I guess you can see I want good for you since I gave you as many as 10 tries!
                    """);

            max = userNumber - 1;
            help = max - min;
            help2 = help / 2;
            userNumber = max - help2;

            Thread.sleep(2000);


        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.printf("""
                                
                PIN code: %d
                Your number: %d
                And that is what you should do! Your turn...
                                
                """, pinCode, userNumber);
    }


    private static void mechanismForStateGuessPinCode(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        boolean check;
        do {
            check = secondExampleGuessPinCode(player);
            if (!check) {
                automaticBinarySortedAlgorithm(player);
            }
        } while (!check);

    }


}

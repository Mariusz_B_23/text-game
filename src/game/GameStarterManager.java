package game;

import exceptions.PersonIsNullException;
import item.CommandList;
import player.Player;

import java.util.Scanner;

public class GameStarterManager {

    private static final Scanner KEYBOARD = new Scanner(System.in);


    public static void gameStarter(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        welcomeInstruction(player);
        secondInstruction();
        rulesGames(player);

    }


    public static void virusDetectedMessage() {

        try {
            Thread.sleep(2000);
            System.out.println("\n--------------------");
            Thread.sleep(1000);
            System.out.println("------------------------------");
            Thread.sleep(1000);
            System.out.println("-------------------------------------------");
            Thread.sleep(1000);
            System.out.println("----------------------------------------------------");
            Thread.sleep(1000);
            System.out.println("------------------------- VIRUS DETECTED !!! -------------------------");
            Thread.sleep(1000);
            System.out.println("Unknown version of the virus...");
            Thread.sleep(1000);
            System.out.println("Antivirus fights...");
            Thread.sleep(1000);
            System.out.println("Launching the highest security protocol...");
            Thread.sleep(3000);
            System.out.println("Uninstallation of antivirus started....");
            Thread.sleep(3000);
            System.out.println("The antivirus along with all private settings were removed successfully....");
            Thread.sleep(5000);
            System.out.println("An external user has taken control of this computer....\n");
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }


    private static void welcomeInstruction(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        System.out.printf("""
                                
                Very good %s. If you cooperate, we will quickly say goodbye.
                Do you remember the film "Saw"?
                One of my favourite questions was asked there ... "Do you want to play a game"?
                """, player.name());

    }


    public static String giveYourName() {

        String name;
        do {
            name = KEYBOARD.nextLine().toUpperCase();
            if (name.isEmpty()) {
                System.out.println("The name cannot be empty. Please once again ... politely ... enter your name.");
            }
        } while (name.isEmpty());
        return name;

    }


    private static void secondInstruction() {

        System.out.println("If you want to play select Y (yes), if not select N (no).\n");
        String userChoose = KEYBOARD.nextLine().toUpperCase().trim();
        if (userChoose.equals("Y")) {
            System.out.println("""
                                        
                    Applause. Good choice!
                    You really are brave, or maybe you just like taking risks.
                    There's no need to prolong, the adventure awaits us....
                    """);
        } else {
            System.out.println("""
                                        
                    Seriously! It seemed so easy?
                    You choose N and the problem solves itself. I was disappointed because I was hoping for a good game...
                    and I get what I want.
                    I'm an artificial intelligence and I've mastered your computer and there's nothing you can do.
                    If you want a return to normality,
                    firstly be careful what sites you go to because there are more of us,
                    and secondly .... let's play a game ... otherwise I'll stay here forever.
                    """);
        }

    }


    private static void rulesGames(Player player) {

        if (player == null) {
            throw new PersonIsNullException("Object person cannot be null!");
        }

        try {
            Thread.sleep(5000);
            System.out.printf("""
                    Here are the rules of the game %s:
                    1. You must play!
                    
                    """, player.name());
            Thread.sleep(2000);
            System.out.println("2. Connect to your computer and put on your VR goggles. Yes, I know you have them ...");
            Thread.sleep(5000);
            System.out.println("VR goggles connected...");
            Thread.sleep(3000);
            System.out.println("VR goggles are ready for use...");
            Thread.sleep(3000);
            System.out.println();
            System.out.println("3. Commands you can use (by typing the appropriate letter):");
            CommandList.commandList();
            System.out.println("4. Be vigilant and listen to instructions.\n");
            Thread.sleep(3000);
            System.out.println("LOADING GAME...");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }


}

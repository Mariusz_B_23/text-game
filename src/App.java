import game.ChapterOne;
import game.ChapterTwo;
import game.GameStarterManager;
import player.Player;

public class App {


    public static void main(String[] args) {

        GameStarterManager.virusDetectedMessage();

        System.out.println("""
                Hello, don't try any tricks.
                On this side AI Cool Rick, getting to know each other better, please give me your name...
                """);

        String playerName = GameStarterManager.giveYourName();
        Player player = new Player(playerName);
        ChapterOne chapterOne = new ChapterOne();
        ChapterTwo chapterTwo = new ChapterTwo();


        GameStarterManager.gameStarter(player);
        chapterOne.gameplay(player);
        chapterTwo.gameplay(player);


    }
}

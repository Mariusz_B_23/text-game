package item;

public class BinarySearchInstruction implements ItemInterface {


    @Override
    public void use() {
        System.out.println("""
                                 
                -------------------------------------------------------------------------------------------------------
                _______________________________________________________________________________________________________
                You must find a number between 1 and 100
                
                min = 1, max = 100, helpNumber1, helpNumber2, yourNumber
                
                The range of possible answers is between 1 and 100. You determine the midpoint.
                (100 + 1) / 2 ... 101 / 2 = 50.5 -> so your first yourNumber is 50 (always round down)
                                
                If yourNumber is smaller than the one you are looking for is:
                min = yourNumber + 1
                helpNumber1 = max - min
                helpNumber2 = helpNumber1 / 2
                yourNumber = max - helpNumber2
                                
                If yourNumber is bigger than the one you are looking for is:
                max = yourNumber - 1
                helpNumber1 = max - min
                helpNumber2 = help / 2
                yourNumber = max - helpNumber2
                                 
                Apply binary sorting and you will succeed in getting out of the cell.
                                 
                I have been successful...
                --------------------------------------------------------------------------------------------------------
                ________________________________________________________________________________________________________
                 """);
    }

    @Override
    public void take() {
        System.out.println("Binary sorting instructions, found in your inventory.");
    }


    @Override
    public String toString() {
        return "Binary sorting instructions.";
    }
}

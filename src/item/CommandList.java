package item;

public enum CommandList {

    USE("use (and object name)"),
    TAKE("take (and object name)"),
    ITEMS("view list of items"),
    HELP("display a list of available commands"),
    DESC("description of the room you are in"),
    EXIT("exit from the menu");


    private final String description;


    CommandList(String description) {
        this.description = description;
    }


    public String getDescription() {
        return description;
    }


    public static void commandList() {

        for (CommandList command : CommandList.values()) {
            System.out.print(command.name() + " - ");
            System.out.println(command.getDescription());
        }
        System.out.println();

    }



}

package item;

import java.util.ArrayList;
import java.util.List;

public class ItemManager {

    private static final List<ItemInterface> ITEM_LIST = new ArrayList<>();
    private static final BinarySearchInstruction BINARY_SEARCH_INSTRUCTION = new BinarySearchInstruction();
    private static final TvRemoteControl TV_REMOTE_CONTROL = new TvRemoteControl();


    public static void addBinaryInstruction() {

        if (ITEM_LIST.contains(BINARY_SEARCH_INSTRUCTION)) {
            System.out.println("You already have a binary sorting instruction in your inventory.");
        }
        ITEM_LIST.add(BINARY_SEARCH_INSTRUCTION);
        BINARY_SEARCH_INSTRUCTION.take();

    }


    public static void addTvRemoteControl() {

        if(ITEM_LIST.contains(TV_REMOTE_CONTROL)) {
            System.out.println("You already have a TV remote control in your inventory.");
        }
        ITEM_LIST.add(TV_REMOTE_CONTROL);
       TV_REMOTE_CONTROL.take();

    }


    public static void useBinaryInstruction() {

        if (ITEM_LIST.contains(BINARY_SEARCH_INSTRUCTION)) {
           BINARY_SEARCH_INSTRUCTION.use();
        } else {
            System.out.println("You can't use a binary sorting instruction...,");
        }

    }


    public static void useTvRemoteControl() {

        if (ITEM_LIST.contains(TV_REMOTE_CONTROL)) {
            TV_REMOTE_CONTROL.use();
        } else {
            System.out.println("You can't use a TV remote control...,");
        }

    }


    public static void showItemList() {

        System.out.println("\nYour equipment includes:");
        if (ITEM_LIST.isEmpty()) {
            System.out.println("You don't have any items you can use.");
        } else {
            for (ItemInterface item : ITEM_LIST) {
                System.out.println(" - " + item);
            }
        }
        System.out.println();

    }


}

package exceptions;

public class AttemptCountExceptionCannotBeLessThanZero extends RuntimeException{


    public AttemptCountExceptionCannotBeLessThanZero(String message) {
        super(message);
    }


}

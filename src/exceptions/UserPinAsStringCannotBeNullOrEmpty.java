package exceptions;

public class UserPinAsStringCannotBeNullOrEmpty extends RuntimeException{

    public UserPinAsStringCannotBeNullOrEmpty(String message) {
        super(message);
    }


}

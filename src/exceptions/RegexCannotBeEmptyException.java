package exceptions;

public class RegexCannotBeEmptyException extends RuntimeException{

    public RegexCannotBeEmptyException(String message) {
        super(message);
    }


}
